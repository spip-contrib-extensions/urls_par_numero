<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/urls_par_numero.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// U
	'configurer_objets' => 'Objets utilisant les urls par numéro',
	'configurer_article' => 'Articles',
	'configurer_formulaire' => 'Formulaires',
	'configurer' => 'Configurer les URLs par numéro',
);

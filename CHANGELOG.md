# Changelog
## 2.2.0 - 2024-08-14

### Added

- Possibilité d'avoir les urls de formulaire en `formulaire/<numero>`
## 2.0.1

### Added

- Compatiblité SPIP 4.2
